
const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
  
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
 
let numOne = document.createElement("div");
numOne.textContent = "Kata 1";
document.body.appendChild(numOne);
function kata1() {
    let newElement = document.createElement("div");
    gotArray = gotCitiesCSV.split(",");
    newElement.textContent = JSON.stringify(gotArray).replace(/,/g, ", ");
    document.body.appendChild(newElement)
    return gotCitiesCSV;
      }
  
kata1();
  
document.write("<br>")

let numTwo = document.createElement("div");
numTwo.textContent = "Kata 2";
document.body.appendChild(numTwo);
function kata2() {
    let newElement = document.createElement("div");
    bestThingArray = bestThing.split(" ");
    newElement.textContent = JSON.stringify(bestThingArray).replace(/,/g, ", ");
    document.body.appendChild(newElement)
    return gotCitiesCSV;
      }
  
kata2();
  
document.write("<br>")

let numThree = document.createElement("div");
numThree.textContent = "Kata 3";
document.body.appendChild(numThree);
function kata3() {
  let newElement = document.createElement("div");
  newGot = gotCitiesCSV.replace(/,/g, "; ");
  newElement.textContent = newGot;
  document.body.appendChild(newElement)
    }

kata3();

document.write("<br>")

let numFour = document.createElement("div");
numFour.textContent = "Kata 4";
document.body.appendChild(numFour);
function kata4() {
  let newElement = document.createElement("div");
  lotrCitiesCSV = lotrCitiesArray.join(", ");
  newElement.textContent = lotrCitiesCSV;
  document.body.appendChild(newElement)
    }

kata4();

document.write("<br>")

let numFive = document.createElement("div");
numFive.textContent = "Kata 5";
document.body.appendChild(numFive);
function kata5() {
  let newElement = document.createElement("div");
  firstFive = lotrCitiesArray.slice(0, 5);
  newElement.textContent = JSON.stringify(firstFive).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata5();

document.write("<br>")

let numSix = document.createElement("div");
numSix.textContent = "Kata 6";
document.body.appendChild(numSix);
function kata6() {
  let newElement = document.createElement("div");
  lastFive = lotrCitiesArray.slice(-5);
  newElement.textContent = JSON.stringify(lastFive).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata6();

document.write("<br>")

let numSeven = document.createElement("div");
numSeven.textContent = "Kata 7";
document.body.appendChild(numSeven);
function kata7() {
  let newElement = document.createElement("div");
  threeToFive = lotrCitiesArray.slice(2, 5);
  newElement.textContent = JSON.stringify(threeToFive).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata7();

document.write("<br>")

let numEight = document.createElement("div");
numEight.textContent = "Kata 8";
document.body.appendChild(numEight);
function kata8() {
  let newElement = document.createElement("div");
  lotrCitiesArray.splice(-6, 1);
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata8();

document.write("<br>")

let numNine = document.createElement("div");
numNine.textContent = "Kata 9";
document.body.appendChild(numNine);
function kata9() {
  let newElement = document.createElement("div");
  lotrCitiesArray.splice(5, 2);
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata9();

document.write("<br>")

let numTen = document.createElement("div");
numTen.textContent = "Kata 10";
document.body.appendChild(numTen);
function kata10() {
  let newElement = document.createElement("div");
  lotrCitiesArray.splice(2, 0, "Rohan");
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata10();

document.write("<br>")

let numEleven = document.createElement("div");
numEleven.textContent = "Kata 11";
document.body.appendChild(numEleven);
function kata11() {
  let newElement = document.createElement("div");
  lotrCitiesArray.splice(2, 0, "Rohan");
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  document.body.appendChild(newElement)
    }

kata11();

document.write("<br>")

let numTwelve = document.createElement("div");
numTwelve.textContent = "Kata 12";
document.body.appendChild(numTwelve);
function kata12() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.slice(0, 14);
  document.body.appendChild(newElement)
    }

kata12();

document.write("<br>")

let numThirteen = document.createElement("div");
numThirteen.textContent = "Kata 13";
document.body.appendChild(numThirteen);
function kata13() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.slice(-12);
  document.body.appendChild(newElement)
    }

kata13();

document.write("<br>")

let numFourteen = document.createElement("div");
numFourteen.textContent = "Kata 14";
document.body.appendChild(numFourteen);
function kata14() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.slice(23,38);
  document.body.appendChild(newElement)
    }

kata14();

document.write("<br>")

let numFifteen = document.createElement("div");
numFifteen.textContent = "Kata 15";
document.body.appendChild(numFifteen);
function kata15() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.substring(69,81);
  document.body.appendChild(newElement)
    }

kata15();

document.write("<br>")

let numSixteen = document.createElement("div");
numSixteen.textContent = "Kata 16";
document.body.appendChild(numSixteen);
function kata16() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.substring(23,38);
  document.body.appendChild(newElement)
    }

kata16();

document.write("<br>")

let numSeventeen = document.createElement("div");
numSeventeen.textContent = "Kata 17";
document.body.appendChild(numSeventeen);
function kata17() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.indexOf("only", 0);
  document.body.appendChild(newElement)
    }

kata17();

document.write("<br>")

let numEighteen = document.createElement("div");
numEighteen.textContent = "Kata 18";
document.body.appendChild(numEighteen);
function kata18() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.indexOf("bit", 0);
  document.body.appendChild(newElement)
    }

kata18();

document.write("<br>")

let numNineteen = document.createElement("div");
numNineteen.textContent = "Kata 19";
document.body.appendChild(numNineteen);
function checkCities(city) {
  if (
    city.includes("aa") || city.includes("ee") || city.includes("ii") || city.includes("oo") || city.includes("uu")
  ) {
    return city;
  }
}
function kata19() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(gotCitiesCSV.split(",")
  .filter(checkCities)
  );
  
  document.body.appendChild(newElement)
}

kata19();
  
document.write("<br>")

let numTwenty = document.createElement("div");
numTwenty.textContent = "Kata 20";
document.body.appendChild(numTwenty);
function checkCities2(city) {
  if (city.slice(-2) === "or") {
    return city;
  }
}
function kata20() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(lotrCitiesArray.filter(checkCities2)).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata20();

document.write("<br>")

let numTwentyOne = document.createElement("div");
numTwentyOne.textContent = "Kata 21";
document.body.appendChild(numTwentyOne);
function checkFirstLetter(word) {
  if (word.charAt(0) === "b") {
    return word;
  }
}
function kata21() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(bestThing.split(" ").filter(checkFirstLetter)).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata21();

document.write("<br>")

let numTwentyTwo = document.createElement("div");
numTwentyTwo.textContent = "Kata 22";
document.body.appendChild(numTwentyTwo);
function checkMirkwood(word) {
  if (word.includes("Mirkwood")) {
    return "Yes";
  } else {
    return "No";
  }
}
function kata22() {
  let newElement = document.createElement("div");
  
  newElement.textContent = checkMirkwood(lotrCitiesArray);
  
  document.body.appendChild(newElement)
}

kata22();

document.write("<br>")

let numTwentyThree = document.createElement("div");
numTwentyThree.textContent = "Kata 23";
document.body.appendChild(numTwentyThree);
function checkHollywood(word) {
  if (word.includes("Hollywood")) {
    return "Yes";
  } else {
    return "No";
  }
}
function kata23() {
  let newElement = document.createElement("div");
  
  newElement.textContent = checkHollywood(lotrCitiesArray);
  
  document.body.appendChild(newElement)
}

kata23();

document.write("<br>")

let numTwentyFour = document.createElement("div");
numTwentyFour.textContent = "Kata 24";
document.body.appendChild(numTwentyFour);
function kata24() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(lotrCitiesArray.indexOf("Mirkwood"));
  
  document.body.appendChild(newElement)
}

kata24();

document.write("<br>")

let numTwentyFive = document.createElement("div");
numTwentyFive.textContent = "Kata 25";
document.body.appendChild(numTwentyFive);
function checkTwoPartCity(city) {
  if (city.includes(" ")) {
    return city;
  }
}
function kata25() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(lotrCitiesArray.filter(checkTwoPartCity)).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata25();

document.write("<br>")

let numTwentySix = document.createElement("div");
numTwentySix.textContent = "Kata 26";
document.body.appendChild(numTwentySix);
function kata26() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(lotrCitiesArray.reverse()).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata26();

document.write("<br>")

let numTwentySeven = document.createElement("div");
numTwentySeven.textContent = "Kata 27";
document.body.appendChild(numTwentySeven);
function kata27() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(lotrCitiesArray.sort()).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata27();

document.write("<br>")

let numTwentyEight = document.createElement("div");
numTwentyEight.textContent = "Kata 28";
document.body.appendChild(numTwentyEight);
function kata28() {
  let newElement = document.createElement("div");
  
  newElement.textContent = JSON.stringify(lotrCitiesArray.sort(function(a, b) {
    return a.length - b.length;
  })).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata28();

document.write("<br>")

let numTwentyNine = document.createElement("div");
numTwentyNine.textContent = "Kata 29";
document.body.appendChild(numTwentyNine);
function kata29() {
  let newElement = document.createElement("div");
  lotrCitiesArray.pop();
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata29();

document.write("<br>")

let numThirty = document.createElement("div");
numThirty.textContent = "Kata 30";
document.body.appendChild(numThirty);
function kata30() {
  let newElement = document.createElement("div");
  lotrCitiesArray.push("Deadest Marshes");
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata30();

document.write("<br>")

let numThirtyOne = document.createElement("div");
numThirtyOne.textContent = "Kata 31";
document.body.appendChild(numThirtyOne);
function kata31() {
  let newElement = document.createElement("div");
  lotrCitiesArray.shift();
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata31();

document.write("<br>")

let numThirtyTwo = document.createElement("div");
numThirtyTwo.textContent = "Kata 32";
document.body.appendChild(numThirtyTwo);
function kata32() {
  let newElement = document.createElement("div");
  lotrCitiesArray.unshift("Rohan");
  newElement.textContent = JSON.stringify(lotrCitiesArray).replace(/,/g, ", ");
  
  document.body.appendChild(newElement)
}

kata32();
